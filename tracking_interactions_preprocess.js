(function(){
    const fs = require('fs')
    const path = require('path');
    
    var initStart = (new Date()).getTime();

    readFile(data => processEvents(eval("[" + data + "]")));

    function processEvents(allEvents){
        var sortedEvents = allEvents.reduce((acc, event) => {
            if(event.path){
                acc[event.path] = acc[event.path] || [];
                acc[event.path].push(event);
            }
            return acc;
        }, {});
    
        var eventsByPath = {};
        var eventsByParent = {};
        for(var field in sortedEvents){
            if(/^\//.test(field)){
                eventsByPath[field] = sortedEvents[field];
            } else if(/^parent:/.test(field)){
                eventsByParent[field.substring(7)] = sortedEvents[field];
            } else {
                console.log("Warning: invalid path " + field, sortedEvents[field]);
            }
        }
    
        filterObjOfArrays(eventsByPath, isValidInteraction);
        filterObjOfArrays(eventsByParent, isValidInteraction);
    
        var eventsByPathAndAction = {};
        for(var field in eventsByPath){
            eventsByPathAndAction[field] = groupEventsByAction(eventsByPath[field]);
        }
    
        var eventsByParentAndAction = {};
        for(var field in eventsByParent){
            eventsByParentAndAction[field] = groupEventsByAction(eventsByParent[field]);
        }

        removeUnnecessaryData(eventsByPathAndAction);
        removeUnnecessaryData(eventsByParentAndAction);

        var resultArr = [
            ("var trackedPaths = " + JSON.stringify(eventsByPathAndAction) + ";"),
            ("var trackedParents = " + JSON.stringify(eventsByParentAndAction) + ";"),
        ];
        writeFile(resultArr.join("\n"));
    
        function filterObjOfArrays(obj, filterFunction){
            for(var field in obj){
                obj[field] = obj[field].filter(filterFunction);
            }
        }
    
        function isValidInteraction(interaction){
            if(!interaction.selector) return false;
            if(!interaction.name) return false;
            if(!interaction.action) return false;
            return true;
        }
    
        function groupEventsByAction(events){
            if(!events){
                return {};
            }
            var eventsByAction = {};
            events.forEach(function(interaction){
                if(!eventsByAction[interaction.action]){
                    eventsByAction[interaction.action] = [];
                }
                eventsByAction[interaction.action].push(interaction);
            });
            return eventsByAction;
        }
    
        var initEnd = (new Date()).getTime();
    
        // console.log("TIME FOR INIT: " + (initEnd - initStart));
    }

    function removeUnnecessaryData(events){
        for(let type in events){
            for(let action in events[type]){
                events[type][action].forEach(interaction => {
                    delete interaction.path;
                });
            }
        }
    }

    function readFile(callback){
        let filePath = path.resolve(__dirname, './preprocess_input.txt');
        fs.readFile(filePath, 'utf8' , (err, data) => {
            if (err) {
                console.error(err.stack);
            }else{
                callback(data);
            }
        });
    }

    function writeFile(data){
        let filePath = path.resolve(__dirname, './preprocess_output.txt');
        fs.writeFile(filePath, data, 'utf8', (err) => {
            if(err){
                console.error(err.stack);
            }else{
                console.log("Output written to " + filePath);
            }
        });
    }
})();